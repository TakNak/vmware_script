#!/bin/sh


ANDROID_COMPILE_SDK="28"
ANDROID_BUILD_TOOLS="28.0.2"
ANDROID_SDK_TOOLS="4333796"

sudo wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip
sudo unzip -d android-sdk-linux android-sdk.zip
sudo chmod -R 0777 android-sdk-linux
sudo chown -R gitlab-runner android-sdk-linux
sudo chgrp -R gitlab-runner android-sdk-linux
echo y | sudo android-sdk-linux/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
echo y | sudo android-sdk-linux/tools/bin/sdkmanager "platform-tools" >/dev/null
echo y | sudo android-sdk-linux/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null
export ANDROID_HOME=$PWD/android-sdk-linux
export ANDROID_SDK_ROOT=$PWD/android-sdk-linux
export PATH=$PATH:$PWD/android-sdk-linux/platform-tools/
#set +o pipefail
yes | sudo android-sdk-linux/tools/bin/sdkmanager --licenses
#set +o pipefail


